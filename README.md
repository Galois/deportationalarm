# DeportationAlarm
Scripts to automate various tasks

## AB_Scraper
- Scrapes recent tweets based on given keywords
- Checks AB for updates
- Sends signal group message with updates
- Require cronjobs to be set up on server

## AirlineAnalyser
- Searches all airports defined in data/airport_icao.txt 
- Compares flights with aircraft list in data/aircrafts.csv
- Print known aircrafts which have been seen in the airport over past 24 hours
