let OnEvent = (doc) => {
    return {
        on: (event, tagName, callback) => {
            doc.addEventListener('click', (event)=>{
                if(event.target.tagName !== tagName) return;
                callback.call(event.target, event);
            }, false);
        }
    }
};
function hightlightFlights()
{
    OnEvent(document).on('onclick', 'TD', function (e) {
        console.log(this.id);
    });
}
// Retrieves a selected file and displays the info in 'flights' table
function retrieveFile()
{
    OnEvent(document).on('onClick', 'LI', function (e) {
        let main_element = document.getElementById("flights");
        let formData = new URLSearchParams(new FormData());
        formData.append("filename", this.id)
        // Post request (date, airport)
        let myInit = {
            method: 'POST',
            body: formData,
        }
        let newRequest = new Request('/files/'+this.id, myInit)
        fetch(newRequest)
            .then(response => { return response.json()})
            .then(reader => {
                for (let i in reader) {
                    let tr_element = document.createElement("tr")
                    let td_element = document.createElement("td")
                    td_element.appendChild(document.createTextNode(reader[i]['firstSeen']))
                    tr_element.appendChild(td_element)
                    td_element = document.createElement("td")
                    td_element.appendChild(document.createTextNode(to_ISO(reader[i]['firstSeen'])))
                    tr_element.appendChild(td_element)
                    td_element = document.createElement("td")
                    td_element.appendChild(document.createTextNode(reader[i]['estDepartureAirport']))
                    tr_element.appendChild(td_element)
                    td_element = document.createElement("td")
                    td_element.appendChild(document.createTextNode(reader[i]['icao24']))
                    tr_element.appendChild(td_element)
                    td_element = document.createElement("td")
                    td_element.appendChild(document.createTextNode(reader[i]['estArrivalAirport']))
                    tr_element.appendChild(td_element)
                    main_element.appendChild(tr_element)
                }
            })
    })

        console.log(this.id);
}
retrieveFile()


function to_ISO(unixtime) {
    let unix_timestamp = unixtime
    let date = new Date(unix_timestamp * 1000);
    let hours = date.getHours();
    let minutes = "0" + date.getMinutes();
    let seconds = "0" + date.getSeconds();

    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();

    let formattedTime = day + '/' + month + '/' + year + ', ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return formattedTime
}

