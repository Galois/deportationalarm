package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"strings"
)


func filter(flights []flight, airlinefile string)(*Aircraft, error){

	data, err := ioutil.ReadFile(airlinefile)
	if err != nil {
		return nil, err
	}

	b := bytes.NewBuffer(data)
	airlines, err := ReadCSV(b)
	if err != nil {
		return nil, err
	}

	if err != nil{
		return nil, err
	}


	for _, f := range flights {
		for _, a := range airlines {
			log.Print(f.Icao24 +" "+a.Icao24)
			if strings.ToLower(f.Icao24) == strings.ToLower(a.Icao24){
				log.Print(a)

				return &a, nil
			}
		}
	}
	return nil, nil
}
