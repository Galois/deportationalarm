package main

import (
	"encoding/csv"
	"io"
)

type Aircrafts []Aircraft

func (aircrafts *Aircrafts) WriteAirline(w io.Writer) error {
	n := csv.NewWriter(w)
	for _, aircraft := range *aircrafts {
		err := n.Write([]string{aircraft.Icao24, aircraft.Company, aircraft.Company})
		if err != nil {
			return err
		}
	}
		n.Flush()
		return n.Error()
	}
