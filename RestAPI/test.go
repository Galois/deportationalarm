package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

var templates *template.Template

type FlightDetails struct {
	Airport  string
	StartDate string
	EndDate string
}

var ReqString = "flights/departure?airport="

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Content-Security-Policy", "safe-inline")

	// If no post send files
	if r.Method != http.MethodPost {

		templates.ExecuteTemplate(w, "index.html", nil)
		return
	}
	// Post form data
	flightDetails := FlightDetails{
		Airport:  r.FormValue("airport"),
		StartDate: r.FormValue("start_date"),
		EndDate: r.FormValue("end_date"),
	}

	if flightDetails.Airport != "" {
		start_date, _ := strconv.Atoi(flightDetails.StartDate)
		end_date, _ := strconv.Atoi(flightDetails.EndDate)
		openResponse, err := callOpenSky(ReqString, flightDetails.Airport, start_date , end_date)
		if err != nil {
			log.Println(err)
		}
		f, err := filter(openResponse, "../scripts/aircrafts.csv")
		if err != nil {
			log.Println(err)
		}
		templates.Execute(w,map[string]interface{}{"found": f} )
		templates.Execute(w, map[string]interface{}{"flights": openResponse} )
		return
		// If Post is not related to form
	}
		}

func FileHandler(w http.ResponseWriter, r *http.Request) {
	files, err := ioutil.ReadDir("./data")
	if err != nil {
		log.Println(err)
	}

	for _, f := range files {
		fmt.Println(f.Name())
	}

	if err != nil {
		fmt.Print(err)
	}

	templates.ExecuteTemplate(w, "index.html", map[string]interface{}{"files": files})
}

type Logger struct {
	handler http.Handler
}

//ServeHTTP handles the request by passing it to the real
//handler and logging the request details
func (l *Logger) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	l.handler.ServeHTTP(w, r)
	log.Printf("%s %s %s", r.Method, r.URL.Path, r.RemoteAddr)
}

//NewLogger constructs a new Logger middleware handler
func NewLogger(handlerToWrap http.Handler) *Logger {
	return &Logger{handlerToWrap}
}

func handleRequest() {
	addr := "localhost:4000"

	myRouter := mux.NewRouter().StrictSlash(true)
	fs := http.FileServer(http.Dir("public/"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	myRouter.HandleFunc("/", HomeHandler)
	myRouter.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./public"))))
	//Request handler
	myRouter.HandleFunc("/files", FileHandler)

	wrappedMux := NewLogger(myRouter)

	log.Printf("server is listening at %s", addr)
	log.Fatal(http.ListenAndServe(addr, wrappedMux))
}

func init() {
}

func main() {
	templates = template.Must(templates.ParseGlob("templates/*.html"))
	handleRequest()
}
