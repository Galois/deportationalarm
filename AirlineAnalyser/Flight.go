package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

// Take list of airports
// create folder for country each airport
// call opensky and get data for specified airport for daterange
// save to folder/airportname_time

type flight struct {
	Icao24                           string `json:"icao24"`
	FirstSeen                        int    `json:"firstSeen"`
	EstDepartureAirport              string `json:"estDepartureAirport"`
	LastSeen                         string `json:"lastSeen"`
	EstArrivalAirport                string `json:"estArrivalAirport"`
	Callsign                         string `json:"callsign"`
	EstDepartureAirportHorizDistance int    `json:"estDepartureAirportHorizDistance"`
	EstDepartureAirportVertDistance  int    `json:"estDepartureAirportVertDistance"`
	EstArrivalAirportHorizDistance   int    `json:"estArrivalAirportHorizDistance"`
	EstArrivalAirportVertDistance    int    `json:"estArrivalAirportVertDistance"`
	DepartureAirportCandidatesCount  int    `json:"departureAirportCandidatesCount"`
	ArrivalAirportCandidatesCount    int    `json:"arrivalAirportCandidatesCount"`
}

//func main(){
//	test := requestFlightAirport("EDSB", 1578342510, 86400)
//	callOpenSky(test)
//}

func callOpenSky(requestString string, airport string, beginTime int, endTime int) ([]flight, error) {
	start := strconv.Itoa(beginTime)
	end := strconv.Itoa(endTime)
	requestStr := requestString + airport + "&begin=" + start + "&end=" + end
	newReq := "https://opensky-network.org/api/" + requestStr
	var flights = []flight{}

	client := &http.Client{}
	req, err := http.NewRequest("GET", newReq, nil)
	if err != nil {
		log.Fatal(err)
	}

	res, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	_ = json.Unmarshal(body, &flights)

	return flights, nil
}
