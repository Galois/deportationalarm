package main

import (
	"encoding/csv"
	"io"
)

type Aircraft struct {
	Icao24 string
	Registration string
	Company string
}


// Read Know Aircraft's file
func ReadCSV(b io.Reader) ([]Aircraft, error) {
	// Create mew reader
	r := csv.NewReader(b)
	var aircrafts []Aircraft

	_, err := r.Read()
	if err !=nil && err != io.EOF {
		return nil, err
	}

	// Process all
	for {
		aircraft, err := r.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}
		// Create each aircraft object and append
		a := Aircraft{aircraft[0], aircraft[1], aircraft[2]}
		aircrafts = append(aircrafts, a)
		}
		return aircrafts, nil
	}

