package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"
)

var ReqString = "flights/departure?airport="
var AirlineFile = "./data/aircrafts.csv"
var fn = "./data/airport_icao.txt"

func main() {

	found := []string{}
	saved := false
	pTime := int(time.Now().Unix() - (60 * 60 * 48))
	cTime := int(time.Now().Unix())

	// Open airport icao file and store in mem
	file, err := os.Open(fn)
	if err != nil {
		log.Println(err)
	}
	defer file.Close()

	r := bufio.NewReader(file)

	log.Printf("from time: %d, until time: %d", pTime, cTime)

	// Loop each line from icao file
	for {
		line, err := r.ReadString('\n')

		if err != nil && err != io.EOF {
			log.Println(err)
			break
		}
		icao := strings.TrimSuffix(line, "\n")

		// Call opensky with request string
		openResponse, err := callOpenSky(ReqString, icao, pTime, cTime)
		if err != nil {
			log.Println(err)
			break
		}

		// open known offender aircraafts and store in mem
		data, err := ioutil.ReadFile(AirlineFile)
		if err != nil {
			log.Println(err)
		}
		b := bytes.NewBuffer(data)
		airlines, err := ReadCSV(b)

		if err != nil {
			log.Println(err)
		}

		// Check if an airline from mem has been at the airport in past x hours
		for _, f := range openResponse {
			for _, a := range airlines {

				if strings.ToLower(f.Icao24) == strings.ToLower(a.Icao24) {

					for _, elem := range found {
						if a.Registration == elem {
							saved = true
						}
					}
					// If unique, print flightradar24 url + reg
					if !saved {
						found = append(found, a.Registration)
						fmt.Printf("https://www.flightradar24.com/data/aircraft/%s\n", a.Registration)
					}
				}
				saved = false
			}
		}
		//	time.Sleep(1*time.Second)
		if icao == "" {
			fmt.Printf("End")
			break
		}
	}

}
