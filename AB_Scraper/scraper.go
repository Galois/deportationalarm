package main

import (
	"github.com/gocolly/colly"
	"log"
	"time"
)

var layout = "2006-01-02"

func search() map[string][]string {
	updates := make(map[string][]string)
	c := colly.NewCollector()
	c.OnXML("//ul/li", func(e *colly.XMLElement) {
		sense := Sentance{e.Text[:]}
		country, t := sense.details()

		if country != "" && t != "" {
			td, err := time.Parse(layout, t)

			if err != nil {
				log.Println(err)
			}

			if time.Now().Before(td) {
				if updates == nil {
					updates[country][0] = td.Format(layout)
				} else {
					updates[country] = append(updates[country], td.Format(layout))
				}
			}
		}
	})
	c.OnRequest(func(r *colly.Request) {
		log.Println("Visiting", r.URL.String())
	})
	c.Visit("https://www.aktionbleiberecht.de/?page_id=10507")
	return updates
}
