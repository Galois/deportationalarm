package main

import (
	_ "fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"syscall"
)

type config struct {
	Number  string `yaml:"Number"`
	GroupID string `yaml:"GroupID"`
}

var c config

func init() {
	log.SetPrefix("TRACE: ")
	log.SetOutput(os.Stderr)
	log.SetFlags(log.Ldate | log.Lmicroseconds)
	c.getConfig()
}

//test := map[string][]string{"Pakistan":{"20.10.2020", "20.10.2020"}, "Tirana/Albanien.":{"20.10.2020", "20.10.2020"}, "Bagladesh":{"11.22.2011"}, "Bakkkla":{"10.20.2002"}, "TestCountry" : {"12.2.2001"}}
func main() {

	new_scrape := search()

	if len(new_scrape) == 0 {
		log.Println("exit")
		syscall.Exit(0)
	}
	checker(new_scrape)
}

func (c *config) getConfig() *config {
	signalconfig, err := ioutil.ReadFile("signal.yaml")
	if err != nil {
		log.Fatal(err)
	}
	err = yaml.Unmarshal(signalconfig, c)
	if err != nil {
		log.Fatal(err)
	}
	return c
}
