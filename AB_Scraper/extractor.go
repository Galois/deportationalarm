package main

import (
	"log"
	"regexp"
	"strings"
	"time"
)

type Extractor interface {
	details() (string, string)
}

type Sentance struct {
	values string
}

// locates and extracts date and country name
func (sentance Sentance) details() (string, string) {
	restring := `(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:20|21)|21\d{2})*`
	countryextract := `(nach)\s([a-zA-Z/]*)`

	term := regexp.MustCompile(countryextract)
	dater := regexp.MustCompile(restring)

	dateindex := dater.FindStringIndex(sentance.values[:])
	stringindex := term.FindStringIndex(sentance.values[:])

	if stringindex != nil && dateindex != nil {
		// Add the country
		country := sentance.values[:][stringindex[0]+5 : stringindex[1]]
		dt := sentance.values[:][dateindex[0]:dateindex[1]]

		fulldate := strings.Split(dt, ".")
		fdt := fulldate[2] + "-" + fulldate[1] + "-" + fulldate[0]
		t, err := time.Parse(layout, fdt)

		if err != nil {
			log.Println(err)
		}
		return country, t.Format(layout)
	}
	return "", ""
}
