package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"reflect"
)

var older = map[string][]string{}
var newer = map[string][]string{}
var mainjson = "scraper.json"

func checker(updated map[string][]string) {

	// Based on AktionBlieb, there should be duplicates of each deportation alert
	// This checks that each deportation if found at least twice and then removes its duplicates
	// Ignores matches which only have one instance as it is false positive.
	for k := range updated {
		log.Println(updated[k])
		if len(updated[k]) > 1 {
			updated[k] = removeDuplicates(updated[k])
		} else {
			delete(updated, k)
		}
	}
	// Read previous deportation alert
	readfile, err := ioutil.ReadFile(mainjson)
	if err != nil {
		log.Println(err)
	}

	// create json respesentation of byte data
	_ = json.Unmarshal(readfile, &older)
	// Check for changes
	eq := reflect.DeepEqual(&older, &updated)
	if eq {
		log.Println("No change")
		os.Exit(0)
	} else {
		for country, dates := range updated {
			_, ok := older[country]
			// if no dates found in old data, add all
			if !ok {
				newer[country] = dates[:]
				log.Println("Add country and date: ", country, dates[:])
			} else {
				for _, v := range dates {
					// if date does not exist, add to map
					if isUnique(older[country], v) {
						newer[country] = append(newer[country], v)
						log.Println("Add date :", newer[country], dates[:])
					}
				}
			}
		}

		newCountry := ""
		for country, i := range newer {
			newCountry = fmt.Sprintf("%v %v %v", newCountry, country, i)
		}
		sendUpdates(newCountry)
		if len(updated) != 0 {
			jsonstring, err := json.Marshal(updated)
			if err != nil {
				log.Println(err)
			}
			_ = ioutil.WriteFile(mainjson, jsonstring, os.ModePerm)
		}
	}
}

// remove any duplicate dates from slice
func removeDuplicates(m []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, v := range m {
		if value := keys[v]; !value {
			keys[v] = true
			list = append(list, v)
		}
	}
	return list
}

// check is a date already exists in a slice
func isUnique(ds []string, d string) bool {
	for _, v := range ds {
		if v == d {
			return false
		}
	}
	return true
}

func sendUpdates(update string) bool {
	log.Println("SENDUOPDATE: " + update)
	if update != "" {
		updatetext := "POSSIBLE UPDATE: from AktionBionbleiberecht\nPlease check the following updates:"
		cmd := exec.Command("signal-cli", "-u", c.Number, "send", "-g", c.GroupID, "-m", updatetext+":\n"+update)
		err := cmd.Run()
		if err != nil {
			log.Println(err)
		}
		log.Println(updatetext + update)
		return true
	}
	return false
}
